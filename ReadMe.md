# Git Practice Tasks

## Create Branches

### First step (Local Repo Creation)

- initialize local git repo on your machine (git init)

### Second Step (main.cpp and ReadMe.md file creation)

```

touch main.cpp                                  # creates main.cpp
touch ReadMe.md                                 # creates ReadMe.cpp

```
 ### Third Step (Stage Changes, commit and Push)

 ```
git add .                                       # stage all the changes
git commit -m "First Commit"                    # Saves all the changes on local machine
git remote add origin " copied url "            # links your local repo to a given remote repo
git push origin master                          # Pushes code to master branch
 ```

 ### Fourth Step (Create Further Branches and adding files respectively)

 ```
git checkout -b dev                             # Creates dev branch and check it out
touch development.txt                           # Creates development.txt file
git add .                                       # stage all the changes
git commit -m "development.txt added"           # Saves all the changes on local machine
git push --set-upstream origin dev              # Pushes code to dev branch


git checkout -b feature/feat_1                  # Creates feature/feat_1 branch and check it out
touch feature_1.cpp                             # Creates feature_1.cpp file
git add .                                       # stage all the changes
git commit -m "feature_1.cpp added"             # Saves all the changes on local machine
git push --set-upstream origin feature/feat_1   # Pushes code to feature/feat_1 branch


git checkout dev                                # Moves back to dev branch
git checkout -b feature/feat_2                  # Creates feature/feat_2 branch and check it out
touch feature_2.cpp                             # Creates feature_2.cpp file
git add .                                       # stage all the changes
git commit -m "feature_2.cpp added"             # Saves all the changes on local machine
git push --set-upstream origin feature/feat_2   # Pushes code to feature/feat_2 branch
 ``` 

## Create Issues and Fixing Them

### Steps to Create Issue
- Click issues in the left scrolling window
- Click on New Issue
- Fill all the necessary fields and press **Create issue**
- Assign the issue to available assignee

### Steps to Fix Issue
- Every issue has its unique id
- After the issue is resolved, mention 'fixes' keyword with issue id in the message of commit command.
- e.g  git commit -m "This fixes #1"

## Rebase
- Rebasing is the process of moving or combining a sequence of commits to a new base commit.
  
```
git checkout dev                                # dev branch is checked out
touch rebaseFile.txt                            # Creates rebaseFile.txt
git add .                                       # stage all the changes
git commit -m "rebaseFile.txt added"            # Saves all the changes on local machine
touch rebaseFile2.txt                           # Creates rebaseFile2.txt
git add .                                       # stage all the changes
git commit -m "rebaseFile2.txt added"           # Saves all the changes on local machine
```
- Dev branch is now 2 commits ahead of feature branches
  
```
git checkout feature/feat_1                     # feature/feat_1 branch is checked out
git rebase dev                                  # reanchor the feature/feat_1 branch against latest changes in dev
git checkout dev                                # dev branch is checkout
git rebase feature/feat_1                       # take feature/feat_1 commits and place on top of the dev branch


git checkout feature/feat_2                     # feature/feat_2 branch is checked out
git rebase dev                                  # all the changes of dev is now reflected in feature/feat_2
git checkout dev                                # dev branch is checkout
git rebase feature/feat_2                       # take feature/feat_2 commits and place on top of the dev branch
```

## Merge

- To merge dev into master

```
git checkout master                             # master branch is checked out
git merge dev                                   #merged dev branch into master branch
```